#!/bin/bash
which jq > /dev/null
if [ $? -gt 0 ]
then
  echo "install jq https://stedolan.github.io/jq/" >&2
  exit 1
fi

required_env='DDS_URL USER_KEY AGENT_KEY'
for reqvar in $required_env
do
  if [ -z "${!reqvar}" ]
  then
    echo "Invalid environment: ${required_env// /, } are required"
    exit 1
  fi
done

check_for_errors() {
  if [ $? -gt 0 ]
  then
    echo "Curl Problem! ${resp}" >&2
    exit 1
  fi
  error=`echo "${resp}" | jq '.error'`
  if [ "${error}" != "null" ]
  then
    echo "API Problem! ${error}" >&2
    exit 1
  fi
}

# Authenticate
resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d '{"agent_key":"'${AGENT_KEY}'","user_key":"'${USER_KEY}'"}' "${DDS_URL}/api/v1/software_agents/api_token"`
check_for_errors
auth_token=`echo "${resp}" | jq -r '.api_token'`

echo "export DDS_AUTH_TOKEN='${auth_token}'"
