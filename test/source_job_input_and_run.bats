#!/usr/bin/env ./test/libs/bats/bin/bats

load 'libs/bats-support/load'
load 'libs/bats-assert/load'
load 'helpers'

export default_input_dir=input
export default_job_input="${default_input_dir}/job.input"

setup() {
  mkdir -p ${BATS_TMPDIR}/${default_input_dir}
  cp ${default_job_input} ${BATS_TMPDIR}/${default_input_dir}
}

teardown() {
  cp ${BATS_TMPDIR}/${default_job_input} input
  rm -rf ${BATS_TMPDIR}/${default_input_dir}
}

script_name='source_job_input_and_run.sh'
script_bin="./bin/${script_name}"

@test "job.input is empty by default" {
  run cat ${default_job_input}
  assert_success
  assert_output ''
}

@test "job.input is writable" {
  echo foo > ${default_job_input}
  run cat ${default_job_input}
  assert_success
  assert_output 'foo'
}

@test "job.input reverts to default after each test" {
  run cat ${default_job_input}
  assert_success
  assert_output ''
}

@test "${script_name} sources ${default_job_input}" {
  echo echo foo > ${default_job_input}
  run $script_bin

  assert_success
  assert_output 'foo'
}

@test "${script_name} sources JOB_INPUT_LOCATION when set" {
  export JOB_INPUT_LOCATION=$(mktemp "${BATS_TMPDIR}/job.input.XXXX")
  echo echo foo > ${default_job_input}
  echo echo bar > ${JOB_INPUT_LOCATION}
  run $script_bin

  assert_success
  assert_output 'bar'
}

@test "${script_name} sources ${default_job_input}, then runs target command" {
  echo echo foo > ${default_job_input}
  run $script_bin echo bar

  assert_success
  assert_line -n 0 'foo'
  assert_line -n 1 'bar'
}

@test "${script_name} respects parameter string quoting" {
  echo echo foo > ${default_job_input}
  run $script_bin echo 'bar  baz' blah

  assert_success
  assert_line -n 0 'foo'
  assert_line -n 1 'bar  baz blah'
}

@test "${script_name} returns failure when target command fails" {
  echo echo foo > ${default_job_input}
  run $script_bin false

  assert_failure
  assert_line -n 0 'foo'
}

@test "${script_name} can export env to target script" {
  echo export FOO=bar > ${default_job_input}
  run $script_bin env

  assert_success
  assert_output -p 'FOO=bar'
}
