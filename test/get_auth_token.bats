#!/usr/bin/env ./test/libs/bats/bin/bats

load 'libs/bats-support/load'
load 'libs/bats-assert/load'
load 'helpers'
load 'curl_mock'

export DDS_URL=http://localhost
export AGENT_KEY=foo
export USER_KEY=bar

teardown() {
  teardown_curl_mocks
}

script_name='get_auth_token.sh'
script_bin="./bin/${script_name}"
curl_params_api_token='-f -s -S -X POST'
env_error_message="Invalid environment: DDS_URL, USER_KEY, AGENT_KEY are required"

function api_token_url() {
  echo "${DDS_URL}/api/v1/software_agents/api_token"
}

@test "${script_name} requires DDS_URL" {
  unset DDS_URL

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires AGENT_KEY" {
  unset AGENT_KEY

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires USER_KEY" {
  unset USER_KEY

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} calls curl api_token and returns error" {
  run $script_bin

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(api_token_url)'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "${script_name} calls curl api_token and returns api error" {
  mock_curl "$(api_token_url)" '{"error": "boom"}'

  run $script_bin

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(api_token_url)'"

  assert_line -n 1 'API Problem! "boom"'
}

@test "${script_name} calls curl api_token and returns token" {
  mock_curl "$(api_token_url)" '{"api_token": "foo"}'

  run $script_bin

  assert_success
  assert_line -n 0 -e "Curling '${curl_params_api_token}[^']*$(api_token_url)'"
  assert_line -n 1 "export DDS_AUTH_TOKEN='foo'"
}
